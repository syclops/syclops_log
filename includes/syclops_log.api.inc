<?php
/**
 * @file
 * Complete Logging API.
 */
/**
 * Constructs message entity based on given field reference objects.
 * @param $message.
 * @param $objects.
 */
function syclops_log_message_generate($message,$objects) {
	// Building the entity.
   $entity = entity_metadata_wrapper('message',$message);
	// Setting field values.
	foreach($objects as $key=>$object) {
		$entity->$key->set($object);		
	}
	// Saving entity.
	$entity->save();		
}
/**
 * Constructs message entity based on given field reference objects.
 * @param $message.
 * @param $params.
 * @param $objects.
 */
function syclops_log_message($type,array $params,array $objects) {
	// Creating message.
	$message = message_create($type,$params);
	// Generate message with given field reference objects.
	syclops_log_message_generate($message,$objects);
}
